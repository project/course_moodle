<?php

class CourseObjectMoodleFile extends CourseObjectMoodleResource {

  public function __construct($values, $entityType) {
    parent::__construct($values, $entityType);

    $this->moodleModuleType = ($values['object_type'] == 'folder') ? 'folder' : 'resource';
  }

}
