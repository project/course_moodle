<?php

class CourseObjectMoodleScorm extends CourseObjectMoodle {

  function poll() {
    global $user;
    $uid = $user->uid;

    $requirement = $this->getOptions();
    $node = node_load($requirement['nid']);
    $cid = $node->course_moodle['cid'];
    //'SCORM/AICC'
    $db = db_select('mdl_scorm', 's')->fields('s', array('id'));
    $db->leftjoin('mdl_course_modules', 'cm', 's.id = cm.instance');
    $db->condition('s.course', $cid);
    $db->condition('s.id', $this->getMoodleInstanceId());
    $scormid = $db->execute()->fetchField();

    $moodle_user = course_moodle_get_moodle_user($uid);

    if ($scormid) {
      //check the scorm records for the user
      $tracked_scorm_complete = $this->scormtrack($moodle_user['id'], $scormid);

      if (is_array($tracked_scorm_complete) && $tracked_scorm_complete['complete']) {
        $this->getFulfillment($user)->setComplete(1)->save();
      }
    }
  }

  /**
   * Track a SCORM object.
   */
  function scormtrack($userid, $scormid) {
    global $_course_moodle_prefix;


    $res = db_query("SELECT * FROM {$_course_moodle_prefix}scorm WHERE id = :scoid", array(':scoid' => $scormid));
    while ($row = $res->fetch()) {
      $scorm = $row;
    }

    $SQL = "SELECT * FROM {$_course_moodle_prefix}scorm_scoes_track WHERE (element LIKE :lesson OR element LIKE :completion) AND (value like :complete OR value like :passed OR value like :finished)  AND scormid = :scormid AND userid = :userid";
    $vars = array(
      ':lesson' => '%lesson_status%',
      ':completion' => '%completion_status%',
      ':complete' => 'complete%',
      ':passed' => 'passed%',
      ':finished' => 'finished%',
      ':scormid' => $scormid,
      ':userid' => $userid
    );
    $resq = db_query($SQL, $vars);
    while ($row = $resq->fetch()) {
      $scormtrack = $row;
    }

    if (isset($scormtrack) && is_object($scormtrack)) {
      $date_completed = NULL;
      $complete = NULL;

      if (trim($scormtrack->value) == 'passed' || trim($scormtrack->value) == 'completed' || trim($scormtrack->value) == 'complete' || trim($scormtrack->value) == 'finished') {
        $date_completed = $scormtrack->timemodified;
        $complete = 1;
      }
      elseif ($scormtrack->value == 'incomplete') {
        $date_completed = NULL;
        $complete = 0;
      }
    }
    else {
      $date_completed = NULL;
      $complete = NULL;
    }

    return array(
      'date_completed' => $date_completed,
      'complete' => $complete,
    );
  }

  public function getTakeUrl() {
    return url('moodle/mod/scorm/view.php', array('query' => array('id' => $this->getInstanceId())));
  }

  function getReport($key) {
    $default = parent::getReport($key);

    $reports = $this->getReports();
    if ($key == 'moodle') {
      $report = $this->getReportInfo();
      return array(
        'title' => $reports[$key]['title'],
        'url' => $report['url'],
      );
    }

    return $default;
  }

  function getReports() {
    $reports = parent::getReports();
    $reports += array(
      'moodle' => array(
        'title' => 'Results',
        'default' => TRUE,
      ),
    );
    return $reports;
  }

  function getMoodleQuestionsEditUrl() {
    return course_moodle_path(TRUE) . "/course/modedit.php?section=0&add=scorm&course={$this->getCourse()->getNode()->course_moodle['cid']}";
  }

}
